package com.orm.entityrelation.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class StudentDto {

    private Integer id;

    @NotBlank(message = "Student name cannot be blank")
    private String studentName;

    private Integer subjectId;

    @Email
    @NotBlank(message = "Student email cannot be blank")
    private String studentEmail;

    @NotBlank(message = "Student number cannot be blank")
    private String mobileNumber;

    private String address;


}
