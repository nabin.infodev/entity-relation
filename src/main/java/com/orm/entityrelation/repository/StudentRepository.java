package com.orm.entityrelation.repository;

import com.orm.entityrelation.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

public interface StudentRepository extends JpaRepository<Student, Integer> {
}
