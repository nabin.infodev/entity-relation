package com.orm.entityrelation.repository;

import com.orm.entityrelation.entity.Subject;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubjectRepository extends JpaRepository<Subject,Integer> {
}
