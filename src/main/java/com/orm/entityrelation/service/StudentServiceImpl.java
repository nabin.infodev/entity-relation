package com.orm.entityrelation.service;

import com.orm.entityrelation.dto.StudentDto;
import com.orm.entityrelation.entity.Student;
import com.orm.entityrelation.entity.Subject;
import com.orm.entityrelation.repository.StudentRepository;
import com.orm.entityrelation.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StudentServiceImpl implements StudentService{

    @Autowired
    private StudentRepository studentRepository;

    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public Student createSubject(StudentDto studentDto) {

        Subject subject = subjectRepository.findById(studentDto.getSubjectId()).orElse(null);

        return studentRepository.save(Student.builder()

                //fields of student class only
                .name(studentDto.getStudentName())
                .mobileNumber(studentDto.getMobileNumber())
                .address(studentDto.getAddress())
                .email(studentDto.getStudentEmail())

                //foreign key
                .subject(subject)

        .build());
    }

    @Override
    public Student updateSubject(StudentDto studentDto) {

        Subject subject = subjectRepository.findById(studentDto.getSubjectId()).orElse(null);

        Student student = studentRepository.findById(studentDto.getId()).orElse(null);

        return studentRepository.save(Student.builder()

                //fields of student class only

                .id(student.getId())
                .name(studentDto.getStudentName())
                .mobileNumber(studentDto.getMobileNumber())
                .address(studentDto.getAddress())
                .email(studentDto.getStudentEmail())

                //foreign key
                .subject(subject)

                .build());
    }

    @Override
    public List<Student> getAll() {
        return studentRepository.findAll();
    }

    @Override
    public Student getDataById(Integer id) {
        return studentRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Integer id) {
         studentRepository.deleteById(id);

    }
}
