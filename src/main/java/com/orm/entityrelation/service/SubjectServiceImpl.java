package com.orm.entityrelation.service;

import com.orm.entityrelation.dto.SubjectDto;
import com.orm.entityrelation.entity.Subject;
import com.orm.entityrelation.repository.SubjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class SubjectServiceImpl  implements SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    @Override
    public Subject createSubject(SubjectDto subjectDto) {
            return subjectRepository.save(Subject.builder()

                    //GETTING DATA FOR NAME FROM DTO (FROM request body in json format)
                    .name(subjectDto.getName())
                    .subjectCode(subjectDto.getSubjectCode())
                    .duration(subjectDto.getDuration())
                    .build());
        }


    @Override
    public Subject updateSubject(SubjectDto subjectDto) {

        Subject subject = subjectRepository.findById(subjectDto.getId()).orElse(null);

        return subjectRepository.save(Subject.builder()

                .id(subject.getId())
                .name(subjectDto.getName())
                .duration(subjectDto.getDuration())
                .subjectCode(subjectDto.getSubjectCode())
                .build());
    }

    @Override
    public List<Subject> getAll() {
        return subjectRepository.findAll();
    }

    @Override
    public Subject getDataById(Integer id) {
        return subjectRepository.findById(id).orElse(null);
    }

    @Override
    public void delete(Integer id) {
         subjectRepository.deleteById(id);


    }
}
