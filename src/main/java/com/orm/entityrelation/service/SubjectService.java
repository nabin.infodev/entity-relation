package com.orm.entityrelation.service;

import com.orm.entityrelation.dto.SubjectDto;
import com.orm.entityrelation.entity.Subject;

import java.util.List;

public interface SubjectService {

    Subject createSubject(SubjectDto subjectDto);

    Subject updateSubject(SubjectDto subjectDto);

    List<Subject> getAll();

    Subject getDataById(Integer id);

    void delete(Integer id);

}
