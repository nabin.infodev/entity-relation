package com.orm.entityrelation.service;

import com.orm.entityrelation.dto.StudentDto;
import com.orm.entityrelation.entity.Student;

import java.util.List;

public interface StudentService {

    Student createSubject(StudentDto studentDto);

    Student updateSubject(StudentDto studentDto);

    List<Student> getAll();

    Student getDataById(Integer id);

    void delete(Integer id);

}
