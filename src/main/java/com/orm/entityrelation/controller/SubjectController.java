package com.orm.entityrelation.controller;

import com.orm.entityrelation.dto.SubjectDto;
import com.orm.entityrelation.entity.Subject;
import com.orm.entityrelation.service.SubjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/subject")
public class SubjectController {

    @Autowired
    private SubjectService subjectService;

    @PostMapping("/save")
    public Subject saveSubject(@RequestBody SubjectDto subjectDto){
        return subjectService.createSubject(subjectDto);
    }

    @PatchMapping("/update")
    public Subject updateSubject(@RequestBody SubjectDto subjectDto){
        return subjectService.updateSubject(subjectDto);
    }

    @GetMapping("/get/all")
    public List<Subject> getAllData(){
        return subjectService.getAll();
    }

    @GetMapping("/get/{subjectId}")
    public Subject getAllData(@PathVariable Integer subjectId){
       return subjectService.getDataById(subjectId);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteData(@PathVariable Integer  id){
        subjectService.delete(id);
    }

}
