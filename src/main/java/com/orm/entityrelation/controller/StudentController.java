package com.orm.entityrelation.controller;

import com.orm.entityrelation.dto.StudentDto;
import com.orm.entityrelation.entity.Student;
import com.orm.entityrelation.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/student")
public class StudentController {


    @Autowired
    private  StudentService studentService;

    @PostMapping("/save")
    public ResponseEntity<?> saveData(@Valid @RequestBody StudentDto studentDto){
        return new ResponseEntity<>(studentService.createSubject(studentDto), HttpStatus.OK);
    }

    @PutMapping("/update")
    public Student updateData(@Valid @RequestBody StudentDto studentDto){
        return studentService.updateSubject(studentDto);
    }

    @GetMapping("/getAll")
    public List<Student> getAllData(){
        return studentService.getAll();
    }

    @GetMapping("/get/id/{id}")
    public Student getBySpecificId(@PathVariable Integer id){
        return studentService.getDataById(id);
    }

    @DeleteMapping("/delete/{id}")
    public void deleteData(@PathVariable Integer id){
         studentService.delete(id);
    }



}
