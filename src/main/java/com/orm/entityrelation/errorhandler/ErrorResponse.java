package com.orm.entityrelation.errorhandler;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@Data
@XmlRootElement(name ="error")
public class ErrorResponse {

    private String errorMessage;

    private List<String> details;

    public ErrorResponse(String errorMessage, List<String> details) {
        this.errorMessage = errorMessage;
        this.details = details;
    }
}
